from flask import Flask
from config import get_app_config
from indexer import doc_indexer
from .api import api


def create_app(app_config=None):
    app = Flask(__name__)

    # setting app config
    app_config = app_config if app_config else get_app_config()
    app.config.from_object(app_config)

    # register blueprints
    app.register_blueprint(api)
    app.register_blueprint(doc_indexer)

    return app
