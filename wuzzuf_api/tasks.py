from . import create_app
from .celery import create_celery
from config import get_app_config
from broker import publish
from adapters.twitter_adapter import TwitterAdapter
from indexer import index_tweets

celery = create_celery(create_app(get_app_config()))


@celery.task
def update_tweets_index_for_user(twitter_username):
    """Responsible for indexing all tweets for a user"""
    twitter = TwitterAdapter()
    index_tweets(twitter.get_tweets_from_user(twitter_username, max_tweets=10000))
    user = twitter.get_user_info(twitter_username)
    if user:
        publish(user)
    return "done: " + twitter_username
