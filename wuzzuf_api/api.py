import requests
from flask import Blueprint, current_app

api = Blueprint('api', __name__)


@api.route('/stats/<username>', methods=['GET'])
def get_user_stats(username):
    # update user tweets index
    from .tasks import update_tweets_index_for_user
    update_tweets_index_for_user.apply_async(args=[username], task_id="stats_for_{}".format(username))
    # return the indexed user stats
    url = "{}/{}".format(current_app.config.get("TWITTER_AGGREGATOR_BASE_URL"), username)
    response = requests.get(url)
    return response.content, response.status_code


@api.route('/tones/<username>', methods=['GET'])
def get_user_tones(username):
    # update user tweets index
    from .tasks import update_tweets_index_for_user
    update_tweets_index_for_user.apply_async(args=[username], task_id="tones_for_{}".format(username))
    # return the indexed user tone analysis
    url = "{}/{}".format(current_app.config.get("TWITTER_ANALYZER_BASE_URL"), username)
    response = requests.get(url)
    return response.content, response.status_code
