import os
from .base import BaseConfig


class StagingConfig(BaseConfig):
    ENV = "production"
    DEBUG = False
    SECRET_KEY = os.environ.get("SECRET_KEY")

    # Dummy Twitter Developer APP Credentials
    TWITTER_CONSUMER_KEY = os.environ.get("TWITTER_CONSUMER_KEY")
    TWITTER_CONSUMER_SECRET = os.environ.get("TWITTER_CONSUMER_SECRET")
    TWITTER_ACCESS_TOKEN_KEY = os.environ.get("TWITTER_ACCESS_TOKEN_KEY")
    TWITTER_ACCESS_TOKEN_SECRET = os.environ.get("TWITTER_ACCESS_TOKEN_SECRET")

    # Celery Configurations
    CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL")
    CELERY_RESULT_BACKEND = os.environ.get("CELERY_RESULT_BACKEND")

    # Message Broker Configurations
    MESSAGE_BROKER_URL = os.environ.get("MESSAGE_BROKER_URL")

    # Twitter Aggregator
    TWITTER_AGGREGATOR_BASE_URL = os.environ.get("TWITTER_AGGREGATOR_BASE_URL")

    # Twitter Account Analyzer
    TWITTER_ANALYZER_BASE_URL = os.environ.get("TWITTER_ANALYZER_BASE_URL")
