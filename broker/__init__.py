import pika
import json
from flask import current_app


def publish(message):
    exchange_name = "users"
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=current_app.config.get("MESSAGE_BROKER_URL")))
    channel2 = connection.channel()
    channel2.exchange_declare(exchange=exchange_name,
                              exchange_type="fanout")
    channel2.basic_publish(exchange=exchange_name, routing_key='', body=json.dumps(message))
