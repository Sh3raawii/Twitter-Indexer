from flask import Blueprint
from adapters.elastic_adapter import ElasticAdapter

doc_indexer = Blueprint('doc_indexer', __name__)

user_tweets_index_name = "user_tweets"
user_tweets_doc_name = "tweet"
user_tweets_index_mapping = {
    "mappings": {
        "tweet": {
            "properties": {
                "tweet_id": {
                    "type": "keyword"
                },
                "text": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "user_id": {
                    "type": "keyword"
                },
                "user_screen_name": {
                    "type": "keyword"
                },
                "user_name": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "created_at": {
                    "type": "date"
                },
                "retweets": {
                    "type": "long"
                },
                "favorites": {
                    "type": "long"
                }
            }
        }
    }
}


@doc_indexer.before_app_first_request
def create_all_indices():
    ElasticAdapter.create_index(user_tweets_index_name, user_tweets_index_mapping)


def index_tweets(tweets):
    tweets = set_doc_id(tweets, 'tweet_id')
    ElasticAdapter.bulk_index(user_tweets_index_name, user_tweets_doc_name, tweets)


def index_tweet(tweet):
    document_id = tweet.get('tweet_id')
    ElasticAdapter.index(user_tweets_index_name, user_tweets_doc_name, document_id, tweet)


def set_doc_id(doc_reader, id_field_name):
    """
    set the _id field to avoid duplicate documents
    :param doc_reader: list/generator of documents
    :param id_field_name: field name with unique value
    :return: doc
    """
    for doc in doc_reader:
        doc['_id'] = doc.get(id_field_name)
        yield doc
